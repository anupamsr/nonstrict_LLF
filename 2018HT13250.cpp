#include <utility>
#include <iomanip>
#include <vector>
#include <iostream>
#include <cstdio>
#include <libgen.h>
#include <climits>
#include <algorithm>
#include <map>
#include <cstdlib>

using namespace std;
unsigned num_decision_points = 0;
struct Task
{
    unsigned id       = 0; // T0 is empty/no-task
    unsigned instance = 1;
    unsigned phase;
    unsigned period;
    unsigned execution_time;
    unsigned deadline;
    unsigned laxity;
    unsigned remaining_time;
    unsigned real_time;
    unsigned release_time;
    unsigned start_time;
    unsigned completion_time;
    bool     is_done = false;
};

void         module_A(FILE         *fp,
                      vector<Task>& task_set);
double       module_B(const vector<Task>& task_set);
unsigned     module_C(const vector<Task>& task_set);
vector<Task> module_D(const vector<Task>& task_set,
                      const unsigned      hyper_period);
void         module_E(const vector<Task>& task_set,
                      const vector<Task>& schedule);
int          main(int argc, char **argv)
{
    char *bname = basename(argv[0]);

    if (argc < 2)
    {
        cerr << "Usage: " << bname << " [FILE]" << endl;
        return EXIT_FAILURE;
    }

    FILE *fp = fopen(argv[1], "r");

    if (fp == nullptr)
    {
        cerr << bname << ": " << argv[1] << ": No such file or directory" << endl;
        return EXIT_FAILURE;
    }

    try
    {
        vector<Task> task_set;
        module_A(fp, task_set);
        auto task_set_utilization = module_B(task_set);
        cout.precision(2);
        cout << "Utilization of the task set: " << task_set_utilization << endl;
        auto hyper_period = module_C(task_set);
        cout << "Hyper period of the task set: " << hyper_period << endl;
        auto schedule = module_D(task_set, hyper_period);
        module_E(task_set, schedule);
    }
    catch (runtime_error& e)
    {
        cerr << bname << ": " << e.what() << endl;
    }
    fclose(fp);
    return EXIT_SUCCESS;
}

void module_A(FILE *fp, vector<Task>& task_set)
{
    char  *line       = NULL;
    size_t len        = 0;
    auto   first_line = true;
    size_t num_tasks  = UINT_MAX;
    size_t i          = 0;

    while (getline(&line, &len, fp) != -1 || i < num_tasks)
    {
        if (first_line)
        {
            if (sscanf(line, "%lu", &num_tasks) == EOF)
            {
                free(line);
                return;
            }

            first_line = false;
        }
        else
        {
            Task task;
            sscanf(line, "%u\t%u\t%u\t%u", &task.phase, &task.period, &task.execution_time, &task.deadline);
            task.id = ++i;
            task_set.emplace_back(task);
        }
    }
    if (i != num_tasks)
    {
        free(line);
        throw runtime_error("Error while processing file");
    }
    for (i = 0; i < num_tasks; ++i)
    {
        Task task = task_set[i];
        cout << "T" << task.id << ": " << task.phase << ", " << task.period << ", " << task.execution_time << ", " << task.deadline << endl;
    }

    free(line);
    return;
}

double module_B(const vector<Task>& task_set)
{
    double task_set_utilization = 0;

    cout.precision(2);
    for (const auto& task : task_set)
    {
        auto utilization = (double)task.execution_time / task.period;
        cout << "Utilization of T" << task.id << ": " << utilization << endl;
        task_set_utilization += utilization;
    }

    return task_set_utilization;
}

unsigned lcm(const unsigned a, const unsigned b)
{
    if (a < b)
    {
        return lcm(b, a);
    }

    unsigned max = a;

    while (true)
    {
        if (max % a == 0 && max % b == 0)
        {
            return max;
        }

        ++max;
    }
}

unsigned module_C(const vector<Task>& task_set)
{
    unsigned hyper_period = task_set[0].period;

    for (const auto& task : task_set)
    {
        hyper_period = lcm(hyper_period, task.period);
    }

    return hyper_period;
}

vector<Task> llf(vector<Task> task_set, const unsigned hyper_period)
{
    vector<Task> schedule(hyper_period);

    for (size_t frame = 0; frame < hyper_period; ++frame)
    {
        cout << "--- Frame " << frame << " ---" << endl;
        vector<Task> frame_tasks;
        for (auto& task : task_set) // Calculate laxity for all valid tasks
        {
            if (frame < task.phase) // skip
            {
                continue;
            }
            else
            {
                task.real_time = (frame - task.phase) % task.period;
                cout << "T" << task.id << ": " << task.real_time << " ";
                if (task.real_time == 0) // Reset at the beginning
                {
                    task.is_done        = false;
                    task.remaining_time = task.execution_time;
                    task.release_time   = frame;
                }
                if (task.is_done)
                {
                    cout << "Skipping" << endl;
                    continue;
                }
                else
                {
                    task.laxity = task.deadline - task.real_time - task.remaining_time;
                    cout << task.laxity << endl;
                    frame_tasks.emplace_back(task);
                }
            }
        }

        // Find the minimum laxity
        vector<Task> minimums;
        unsigned     min_laxity = UINT_MAX;
        for (const auto& task : frame_tasks)
        {
            if (task.laxity < min_laxity)
            {
                min_laxity = task.laxity;
                minimums   = { task };
            }
            else if (task.laxity == min_laxity) // Check if multiple tasks have minimum laxity
            {
                minimums.emplace_back(task);
            }
        }

        cout << "Minimums: ";
        for (const auto& task : minimums)
        {
            cout << "T" << task.id << ": " << task.laxity << ", ";
        }

        cout << endl;

        // Find the task to be scheduled
        if (minimums.size() == 0)          // There are no minimum tasks
        {
            schedule[frame] = Task();      // Nothing to schedule
        }
        else if (minimums.size() == 1)     // We found 1 minimum.
        {
            schedule[frame] = minimums[0]; // That task will be scheduled.
        }
        else // We found more than 1 minimum.
        {
            bool scheduled = false;
            if (frame > 0)
            {
                auto it = find_if(minimums.begin(), minimums.end(), [&](const Task& task)-> bool {
                    return task.id == schedule[frame - 1].id;
                });
                if (it != minimums.end())                  // Previous task is among the minimum
                {
                    schedule[frame] = schedule[frame - 1]; // Continue scheduling previous task
                    scheduled       = true;
                }
            }
            if (!scheduled) // If previous task is not minimum anymore or this is the first frame
            {
                vector<size_t> released_earlier = { 0 };
                unsigned max_real_time = minimums[0].real_time;
                for (size_t i = 1; i < minimums.size(); ++i) // Pick one which was released earlier
                {
                    if (max_real_time < minimums[i].real_time)
                    {
                        max_real_time    = minimums[i].real_time;
                        released_earlier = { i };
                    }
                    else if (max_real_time == minimums[i].real_time)
                    {
                        released_earlier.emplace_back(i);
                    }
                }

                schedule[frame] = minimums[released_earlier[0]]; // Ideally we should pick randomly.
            }
        }
        if (schedule[frame].id != 0)
        {
            cout << "Picking T" << schedule[frame].id << endl;
        }

        // Reduce executed time
        auto it = find_if(task_set.begin(), task_set.end(), [&](const Task& task) -> bool {
            return task.id == schedule[frame].id;
        });
        --it->remaining_time;
        if (it->remaining_time == 0)
        {
            it->is_done = true;
            ++it->instance;
        }
    }

    return schedule;
}

vector<Task> nsllf(vector<Task> task_set, const unsigned hyper_period)
{
    vector<Task> schedule(hyper_period);

    for (size_t frame = 0; frame < hyper_period; ++frame)
    {
        cout << "--- Frame " << frame << " ---" << endl;
        bool take_decision = false;
        if (frame == 0)
        {
            cout << "Preempting because first frame" << endl;
            take_decision = true;
            ++num_decision_points;
        }
        else
        {
            auto it = find_if(task_set.begin(), task_set.end(), [&](const Task& task) -> bool {
                return task.id == schedule[frame - 1].id;
            });
            if (it->is_done) // Running task has finished
            {
                cout << "Preempting because last task T" << it->id << " is done" << endl;
                take_decision = true;
                ++num_decision_points;
            }
            else // Check if any task is being released
            {
                for (auto& task : task_set)
                {
                    if (frame < task.phase) // skip
                    {
                        continue;
                    }
                    else
                    {
                        auto real_time = (frame - task.phase) % task.period;
                        if (real_time == 0) // At least one task is being released
                        {
                            cout << "Preempting because T" << task.id << " is released" << endl;
                            take_decision = true;
                            ++num_decision_points;
                            break;
                        }
                    }
                }
            }
        }
        if (take_decision)
        {
            vector<Task> frame_tasks;
            for (auto& task : task_set) // Calculate laxity for all valid tasks
            {
                if (frame < task.phase) // skip
                {
                    continue;
                }
                else
                {
                    task.real_time = (frame - task.phase) % task.period;
                    cout << "T" << task.id << ": " << task.real_time << " ";
                    if (task.real_time == 0) // Reset at the beginning
                    {
                        task.is_done        = false;
                        task.remaining_time = task.execution_time;
                        task.release_time   = frame;
                    }
                    if (task.is_done)
                    {
                        cout << "Skipping" << endl;
                        continue;
                    }
                    else
                    {
                        task.laxity = task.deadline - task.real_time - task.remaining_time;
                        cout << task.laxity << endl;
                        frame_tasks.emplace_back(task);
                    }
                }
            }

            // Find the minimum laxity
            vector<Task> minimums;
            unsigned     min_laxity = UINT_MAX;
            for (const auto& task : frame_tasks)
            {
                if (task.laxity < min_laxity)
                {
                    min_laxity = task.laxity;
                    minimums   = { task };
                }
                else if (task.laxity == min_laxity) // Check if multiple tasks have minimum laxity
                {
                    minimums.emplace_back(task);
                }
            }

            cout << "Minimums: ";
            for (const auto& task : minimums)
            {
                cout << "T" << task.id << ": " << task.laxity << ", ";
            }

            cout << endl;

            // Find the task to be scheduled
            if (minimums.size() == 0)          // There are no minimum tasks
            {
                schedule[frame] = Task();      // Nothing to schedule
            }
            else if (minimums.size() == 1)     // We found 1 minimum.
            {
                schedule[frame] = minimums[0]; // That task will be scheduled.
            }
            else // We found more than 1 minimum.
            {
                bool scheduled = false;
                if (frame > 0)
                {
                    auto it = find_if(minimums.begin(), minimums.end(), [&](const Task& task)-> bool {
                        return task.id == schedule[frame - 1].id;
                    });
                    if (it != minimums.end())                  // Previous task is among the minimum
                    {
                        schedule[frame] = schedule[frame - 1]; // Continue scheduling previous task
                        scheduled       = true;
                    }
                }
                if (!scheduled) // If previous task is not minimum anymore or this is the first frame
                {
                    vector<size_t> released_earlier = { 0 };
                    unsigned max_real_time = minimums[0].real_time;
                    for (size_t i = 1; i < minimums.size(); ++i) // Pick one which was released earlier
                    {
                        if (max_real_time < minimums[i].real_time)
                        {
                            max_real_time    = minimums[i].real_time;
                            released_earlier = { i };
                        }
                        else if (max_real_time == minimums[i].real_time)
                        {
                            released_earlier.emplace_back(i);
                        }
                    }

                    schedule[frame] = minimums[released_earlier[0]]; // Ideally we should pick randomly.
                }
            }
        }
        else
        {
            schedule[frame] = schedule[frame - 1];
        }
        if (schedule[frame].id != 0)
        {
            cout << "Picking T" << schedule[frame].id << endl;
        }

        // Reduce executed time
        auto it = find_if(task_set.begin(), task_set.end(), [&](const Task& task) -> bool {
            return task.id == schedule[frame].id;
        });
        --it->remaining_time;
        if (it->remaining_time == 0)
        {
            it->is_done = true;
            ++it->instance;
        }
    }

    return schedule;
}

vector<Task> module_D(const vector<Task>& task_set, const unsigned hyper_period)
{
    //    auto schedule = llf(task_set, hyper_period);
    auto schedule = nsllf(task_set, hyper_period);

    for (const auto& task : schedule)
    {
        cout << "| " << (task.id == 0 ? " - " : "T" + to_string(task.id) + to_string(task.instance)) << " ";
    }

    cout << "|" << endl;
    for (size_t i = 0; i <= schedule.size(); ++i)
    {
        cout << setw(6) << std::left << i;
    }

    cout << endl;
    return schedule;
}

void module_E(const vector<Task>& task_set, const vector<Task>& schedule)
{
    vector<bool> calculated(schedule.size(), false);
    vector<Task> instances;

    for (size_t i = 0; i < schedule.size(); ++i)
    {
        if (!calculated[i])
        {
            instances.emplace_back(schedule[i]);
            auto task = &instances.back();
            calculated[i]        = true;
            task->start_time     = i;
            task->remaining_time = task->execution_time;
            for (size_t j = i; j < schedule.size(); ++j)
            {
                if (schedule[i].id == schedule[j].id && schedule[i].instance == schedule[j].instance)
                {
                    --task->remaining_time;
                    calculated[j] = true;
                }
                if (task->remaining_time == 0)
                {
                    task->completion_time = j + 1;
                    cout << "T" << task->id << task->instance << ": r = " << task->release_time << ", s = " << task->start_time << ", c = " << task->completion_time << endl;
                    break;
                }
            }
        }
    }

    vector<pair<unsigned, unsigned> > absolute_response_time_jitters;
    vector<pair<unsigned, int> > relative_response_time_jitters;

    for (const auto& task : task_set)
    {
        vector<unsigned> response_times;
        for (const auto& instance : instances)
        {
            if (task.id == instance.id)
            {
                response_times.emplace_back(instance.completion_time - instance.release_time);
            }
        }

        auto max_response_time = *max_element(response_times.begin(), response_times.end());
        auto min_response_time = *min_element(response_times.begin(), response_times.end());
        cout << "Maximum response time of T" << task.id << ": " << max_response_time << endl;
        cout << "Minimum response time of T" << task.id << ": " << min_response_time << endl;
        absolute_response_time_jitters.emplace_back(make_pair(task.id, max_response_time - min_response_time));
        vector<int> relative_response_times;
        for (size_t i = 0; i < response_times.size(); ++i)
        {
            auto response_time_i_1 = response_times[(i + 1) % response_times.size()];
            auto response_time_i   = response_times[i % response_times.size()];
            relative_response_times.emplace_back(response_time_i_1 - response_time_i);
        }

        relative_response_time_jitters.emplace_back(make_pair(task.id, *max_element(relative_response_times.begin(), relative_response_times.end())));
    }
    for (const auto& p : absolute_response_time_jitters)
    {
        cout << "Absolute response time jitter of T" << p.first << ": " << p.second << endl;
    }
    for (const auto& p : relative_response_time_jitters)
    {
        cout << "Relative response time jitter of T" << p.first << ": " << p.second << endl;
    }

    cout << "Number of Decision points: " << num_decision_points << endl;
    unsigned num_context_switches = 0;

    for (size_t i = 1; i < schedule.size(); ++i)
    {
        if (schedule[i].id != schedule[i - 1].id)
        {
            ++num_context_switches;
        }
    }

    cout << "Number of Context Switches: " << num_context_switches << endl;
    for (const auto& task : task_set)
    {
        vector<unsigned> latencies;
        for (const auto& instance : instances)
        {
            if (task.id == instance.id)
            {
                latencies.emplace_back(instance.completion_time - instance.start_time);
            }
        }

        cout << "Maximum latency of T" << task.id << ": " << *max_element(latencies.begin(), latencies.end()) << endl;
        cout << "Minimum latency of T" << task.id << ": " << *min_element(latencies.begin(), latencies.end()) << endl;
    }
}
