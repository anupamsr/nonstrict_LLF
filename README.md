# nonstrict_LLF

This program implements a rather non-optimized way to schedule tasks based on non-strict LLF.

As per "Real-Time Systems" by Jane W. S. Liu, (ISBN-10: 0130996513, ISBN-13: 978-0130996510)
```
Because scheduling decisions are made only at the times when jobs are released or com-
pleted, this version of the LST algorithm does not follow the LST rule of priority assignment
at all times. If we wish to be specific, we should call this version of the LST algorithm the
nonstrict LST algorithm
```

A function implementing LLF is also implemented but not used in actual execution.
